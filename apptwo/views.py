from django.shortcuts import render
from django.http import HttpResponse
import datetime

# Create your views here.


def index(request):
    return HttpResponse("<h1>THIS IS THE HOME PAGE</h1>")


def help(request):
    return render(request, 'apptwo/help.html')


def template_view(request):
    dt = datetime.datetime.now()
    name = "PRABESH"
    rollno = 24
    marks = 100
    my_dict = {'date': dt, 'name': name, 'rollno': rollno, 'marks': marks}
    return render(request, 'apptwo/student.html', my_dict)
